#include <SFML/Graphics.hpp>
#include <time.h>
#include <SFML/Audio.hpp>
#include "structures.hpp"
using namespace sf;

//signature des fonctions

void affichagelevel(RenderWindow& menufenetre);

void affichagemenu(RenderWindow& menufenetre);

void score(RenderWindow & fenetre,int ennemyK,int shotFired, int &point);

int hpLeft (int &healthPoint,int damage,RenderWindow &fenetre);

void affichagefin(RenderWindow& fenetre);

void affichagefin2(RenderWindow& fenetre);

void processMouvement(Ship &theShip, Level &One);
