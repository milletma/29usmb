#include <SFML/Graphics.hpp>
#include <iostream>
#define SIZE_WINDOW 800
#define SIZE_SHIP 80
#define SIZE_SHIP2 80
#define STEP 10

using namespace sf;
typedef struct
{
    int posX;
    int posY;
} Ship;

int main()
{
    Ship ship, ship3;
    int centre = (SIZE_WINDOW - SIZE_SHIP)/2;
    ship.posX = centre;
    ship.posY = centre;

    RenderWindow fenetre(VideoMode(SIZE_WINDOW, SIZE_WINDOW), "SPACE SHOOTER");

    Texture image2;
    if (!image2.loadFromFile("ennemyship.png"))
        printf("PB de chargement de l'image !\n");
    /* L�image doit �tre associ�e � un objet de type Sprite (�quivalent � un rectangle) pour �tre dessin�e. Ici , le sprite s�appelle bonhomme. Apr�s, on ne travaille plus que sur le sprite� image ne sert plus � rien*/
    Sprite ship2;
    ship2.setTexture(image2);
    ship2.setPosition(ship3.posX,ship3.posY);
    fenetre.draw(ship2);


    Texture image;
    if (!image.loadFromFile("ship.png"))
        printf("PB de chargement de l'image !\n");
    /* L�image doit �tre associ�e � un objet de type Sprite (�quivalent � un rectangle) pour �tre dessin�e. Ici , le sprite s�appelle bonhomme. Apr�s, on ne travaille plus que sur le sprite� image ne sert plus � rien*/
    Sprite ship1;
    ship1.setTexture(image);
    ship1.setPosition(ship.posX,ship.posY);
    fenetre.draw(ship1);
    fenetre.display();

    Event evenement;
    while (fenetre.isOpen())
    {
        ship3.posX=300;
        ship3.posY=200;

        if ((ship.posX+SIZE_SHIP == ship3.posX) && ((ship.posY>=ship3.posY-SIZE_SHIP)&&(ship.posY<=ship3.posY+SIZE_SHIP)))
        {
            ship.posX=centre;
            ship.posY=centre;
            printf("Collision a droite");
        }

        else if ((ship.posX == ship3.posX+SIZE_SHIP) && ((ship.posY>=ship3.posY-SIZE_SHIP)&&(ship.posY<=ship3.posY+SIZE_SHIP)))
        {
            ship.posX=centre;
            ship.posY=centre;
            printf("Collision a gauche");
        }
        else if (((ship.posX>=ship3.posX-SIZE_SHIP)&&(ship.posX<=ship3.posX+SIZE_SHIP))&&(ship.posY == ship3.posY+SIZE_SHIP))
        {
            ship.posX=centre;
            ship.posY=centre;
            printf("Collision en haut");
        }
        else if (((ship.posX>=ship3.posX-SIZE_SHIP)&&(ship.posX<=ship3.posX+SIZE_SHIP))&&(ship.posY+SIZE_SHIP == ship3.posY))
        {
            ship.posX=centre;
            ship.posY=centre;
            printf("Collision en bas");
        }

        fenetre.clear(Color::Black);
        while (fenetre.pollEvent(evenement))
        {

            switch (evenement.type)
            {
            case Event::Closed:
                fenetre.close();
                break;
            case Event::KeyPressed:
                if (evenement.key.code == Keyboard::Up)
                {
                    if (ship.posY != 100)
                    {
                        printf("haut\n");
                        ship.posY = ship.posY-STEP;
                    }
                    else
                        printf("erreur bordure\n");
                }
                else if (evenement.key.code == Keyboard::Down)
                {
                    if (ship.posY != SIZE_WINDOW-SIZE_SHIP)
                    {
                        printf("bas\n");
                        ship.posY = ship.posY+STEP;
                    }
                    else
                        printf("erreur bordure\n");
                }
                else if (evenement.key.code == Keyboard::Left)
                {
                    if (ship.posX != 0)
                    {
                        printf("gauche\n");
                        ship.posX = ship.posX-STEP;
                    }
                    else
                        printf("erreur bordure\n");
                }
                else if (evenement.key.code == Keyboard::Right)
                {
                    if (ship.posX != SIZE_WINDOW-SIZE_SHIP)
                    {
                        printf("droite\n");

                        ship.posX = ship.posX+STEP;
                    }
                    else
                        printf("erreur bordure\n");
                }
                else if (evenement.key.code == Keyboard::Space)
                {
                    printf("centre\n");
                    ship.posX = centre;
                    ship.posY = centre;
                }
                ship1.setTexture(image);
                ship1.setPosition(ship.posX,ship.posY);
                ship2.setTexture(image2);
                ship2.setPosition(ship3.posX,ship3.posY);
                fenetre.draw(ship1);
                fenetre.draw(ship2);
                fenetre.display();
                break;
            }
        }
    }
    return 0;
}
