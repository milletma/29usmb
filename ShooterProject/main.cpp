#include <SFML/Graphics.hpp>
#define TAILLE_FENETRE 800

using namespace sf;

int main()
{
    // Create the main window
    RenderWindow app(VideoMode(TAILLE_FENETRE, TAILLE_FENETRE), "SFML window");

    // Load a sprite to display
    Texture texture;
    if (!texture.loadFromFile("VAISSEAU"))
        return EXIT_FAILURE;
    Sprite sprite(texture);

	// Start the game loop
    while (app.isOpen())
    {
        // Process events
        Event event;
        while (app.pollEvent(event))
        {

            // Close window : exit
            if (event.type == Event::Closed)
                app.close();
        }

        // Clear screen
        app.clear();
        app.draw(sprite);
        app.display();
    }

    return EXIT_SUCCESS;
}
