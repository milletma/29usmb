#include <SFML/Graphics.hpp>
#define TAILLE_FENETRE 800
#define HP_4 "HP4.png"
#define HP_3 "HP3.png"
#define HP_2 "HP2.png"
#define HP_1 "HP1.png"
#define NB_SHIPS_EASY 15
#define NB_SHIPS_MEDIUM 20
#define NB_SHIPS_HARD 20


using namespace sf;
int hpLeft (int HP,int damage,int ship,int deadEnd,int level,int restart,int uWin,RenderWindow & app);



int main()
{
    // Create the main window
    RenderWindow app(VideoMode(TAILLE_FENETRE, TAILLE_FENETRE), "SpaceShooter");

    // Load a sprite to display
    Texture texture;
    Sprite sprite(texture);

    // Start the game loop
    while (app.isOpen())
    {
        // Process events
        Event event;
        while (app.pollEvent(event))
        {

            // Close window : exit
            if (event.type == Event::Closed)
                app.close();
        }

        // Clear screen
        app.clear();
        hpLeft(2,0,1,0,2,0,2,app);
        //app.draw(sprite);
        //app.display();
    }
    return 0;
}

int hpLeft (int HP,int damage,int ship,int deadEnd,int level,int restart,int uWin,RenderWindow & app)
{
    /*
    Lorsque ship=1, permet de dire de continuer dafficher le ship. Si 0, arrete de l'affichage
    deadEnd a 1 = fin de partie
    */
      Font font;
    if (!font.loadFromFile("techno_hideo.ttf"))
    {
        printf("Fatal error: techno_hideo.ttf missing");

    }

   Text text;
   text.setFont(font);


    Text HPdis, textDisplay;
    textDisplay.setFont(font);
    textDisplay.setPosition(50,700);
    char HPwrite [50];
    sprintf(HPwrite,"HP left :");
    HPdis.setString(HPwrite);
    HPdis.setFont(font);
    HPdis.setStyle(Text::Bold);
    //HPdis.setFillColor(Color::);
    HPdis.setPosition(20,700);

    app.draw(HPdis);


    Sprite s;
    Texture t;

    if (level==1)
    {
        HP=4;
           // NB_SHIPS_EASY;
    }

    if (level==2)
    {
        HP=3;
        //NB_SHIPS_MEDIUM;
    }

    if (level==3)
    {
        HP=1;
        // NB_SHIPS_HARD;
    }

    else if (level >=4)
    {
        printf("FATAL error, level is not 1,2 or 3");
        system("pause");
        restart=1;
    }

    if (HP==4)
    {
        ship=1;

        if (!t.loadFromFile(HP_4))
            printf("WARNING , can t load  %s !\n", HP_4 );

        s.setPosition(20,740);

        if (damage==1)
        {
            HP=HP-1;
            printf("You have been hit !");
            damage=damage-1;
        }
    }

    if (HP==3)
    {
        ship=1;

        if (!t.loadFromFile(HP_3))
            printf("WARNING , can t load %s !\n", HP_3 );

        s.setPosition(20,740);

        if (damage==1)
        {
            HP=HP-1;
            printf("You have been hit !");
            damage=damage-1;
        }

    }

    if  (HP==2)
    {
        ship =1;

        if (!t.loadFromFile(HP_2))
            printf("WARNING , can t load %s !\n", HP_2);

            s.setPosition(20,740);

        if (damage==1)
        {
            HP=HP-1;
            printf("You have been hit !");
            damage=damage-1;
        }
    }

    if (HP==1)
    {

        if (!t.loadFromFile(HP_1))
            printf("WARNING , can t load %s !\n", HP_1);

        s.setPosition(20,740);


        if (damage==1)
        {
            HP=HP-1;
            printf("You have been hit !");
            damage=damage-1;
        }

    }

    else if (HP==0)
    {
        ship=0;
        deadEnd=1;

    }
if (uWin==1)
    {
       ship=0;

    }

else if (uWin==0)
{
    ship=0;
        printf("Defaite\n");

}

   s.setTexture(t);
         app.draw(s);
        app.display();


   return HP;

}
