#include <SFML/Graphics.hpp>
#include <time.h>
#define SIZE_WINDOW 800
#define WIDTH_ENNEMY 60
#define HEIGHT_ENNEMY 60
#define PICTURE_NAME "ennemyship.png"
#define PICTURE_NAME_TWO "ennemyshiptwo.png"
#define STEP 1
#define PAUSE 10
#define NB_SHIPS_EASY 15
using namespace sf;

int main ()
{
    srand(time(NULL));
    int nb, choix, i;
    int nbEShip = 0;
    int PositionY = 0;
    RenderWindow fenetre(VideoMode(SIZE_WINDOW, SIZE_WINDOW), "ENNEMY SHIP");
    Texture picture;
    if (!picture.loadFromFile(PICTURE_NAME))
        printf("PB de chargement de l'image %s !\n", PICTURE_NAME);
    Sprite ennemyOne;
    Texture pictureTwo;
    if (!pictureTwo.loadFromFile(PICTURE_NAME_TWO))
        printf("PB de chargement de l'image %s !\n", PICTURE_NAME_TWO);
    Sprite ennemyTwo;
    //picture.setSmooth(true);
    //pictureTwo.setSmooth(true);

    nb = rand() %(SIZE_WINDOW-WIDTH_ENNEMY);
    Event evenement;

    while (fenetre.isOpen())
    {
        for (i=0;i<NB_SHIPS_EASY;i++)
    {
        if (PositionY == 0)
            {
                choix = rand()%2+1;

                if (choix == 1)
                {
                    while (PositionY != SIZE_WINDOW)
                    {
                        fenetre.clear(Color::Black);
                        PositionY = PositionY+STEP;
                        ennemyOne.setTexture(picture);
                        ennemyOne.setPosition(nb,PositionY);
                        fenetre.draw(ennemyOne);
                        fenetre.display();
                    }
                    sleep(milliseconds(PAUSE));
                    nb = rand() %(SIZE_WINDOW-WIDTH_ENNEMY);
                    fenetre.clear(Color::Black);
                    PositionY = 0;
                    nbEShip = nbEShip+1;
                    //printf("%i\n",nbEShip); Permet de v�rifier le compteur de boucle
                    ennemyOne.setTexture(picture);
                    ennemyOne.setPosition(nb,PositionY);
                    fenetre.draw(ennemyOne);
                    fenetre.display();
                }
                else
                {
                    while (PositionY != SIZE_WINDOW)
                    {
                        fenetre.clear(Color::Black);
                        PositionY = PositionY+STEP;
                        ennemyTwo.setTexture(pictureTwo);
                        ennemyTwo.setPosition(nb,PositionY);
                        fenetre.draw(ennemyTwo);
                        fenetre.display();
                    }
                    sleep(milliseconds(PAUSE));
                    nb = rand() %(SIZE_WINDOW-WIDTH_ENNEMY);
                    fenetre.clear(Color::Black);
                    PositionY = 0;
                    nbEShip = nbEShip+1;
                    //printf("%i\n",nbEShip); Permet de v�rifier le compteur de boucle
                    ennemyTwo.setTexture(pictureTwo);
                    ennemyTwo.setPosition(nb,PositionY);
                    fenetre.draw(ennemyTwo);
                    fenetre.display();
                }
            }
        }
        break;
    }
    return 0;
}
