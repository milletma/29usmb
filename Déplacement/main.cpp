#include <SFML/Graphics.hpp>
#include <time.h>
#define WIDTH_ENNEMY 60
#define HEIGHT_ENNEMY 60
#define SIZE_WINDOW 800
#define SIZE_SHIP 80
#define SIZE_BULLET_height 60
#define SIZE_BULLET_width 20


using namespace sf;

//signature des fonctions
void affichagelevel(RenderWindow& menufenetre);
void affichagemenu (RenderWindow& menufenetre);

//initialisation des structures
typedef struct
{
    float posX;
    float posY;
    int speed;

} Bullet;

typedef struct
{

    float posX;
    float posY;

} Ship;

typedef struct
{

    float step;
    float speedShipEnnemy;
    int healthPoint;
    int numberEnnemy;

} Level;


int main()
{

    //d�claration des structures
    Bullet infoBullet;
    Ship theShip;
    Ship ennemyShip;

    Level One;

    //d�claration des variables
    int centre = (SIZE_WINDOW - SIZE_SHIP)/2;
    int choix, i, test;


    //initialisation des sprites et des textures
    Texture image;
    if (!image.loadFromFile("img/ship.png"))
        printf("Error loading ship pitcure !\n");
    Sprite shipInGame;
    shipInGame.setTexture(image);
    shipInGame.setPosition(theShip.posX,theShip.posY);

    Texture imageBullet;
    if (!imageBullet.loadFromFile("img/bullet.png"))
        printf("Error loading ship bullet picture !");
    Sprite bulletShip;
    bulletShip.setTexture(imageBullet);
    bulletShip.setPosition(infoBullet.posX,infoBullet.posY);

    Texture picture;
    if (!picture.loadFromFile("img/ennemyship.png"))
        printf("Error loading ennemyship picture !\n");
    Sprite ennemyOne;

    Texture pictureTwo;
    if (!pictureTwo.loadFromFile("img/ennemyshiptwo.png"))
        printf("Error loading ennemyshiptwo picture !\n");
    Sprite ennemyTwo;

    Texture background;
    if(!background.loadFromFile("img/bckgrd.jpg"));
    printf("Error loading background picture !");
    Sprite backgrd;


    //initialisation des positions de d�parts
    theShip.posX = centre;
    theShip.posY = centre+(centre/2);
    ennemyShip.posX = 0;
    ennemyShip.posY = 0;
    infoBullet.posX = theShip.posX;
    infoBullet.posY = -60;

    //d�claration des autres trucs
    Event evenement;
    srand(time(NULL));


    //d�claration des shapes pour la 1er fenetre
    RectangleShape diff(Vector2f (250, 100));
    diff.setFillColor(Color(255, 255, 255));
    diff.setPosition(280, 350);

    //d�claration des variables pour la 2ieme fenetre
    int x;
    int y;
    int level;
    int fenetreaffiche=0;


    RenderWindow menufenetre(VideoMode(800, 800), "fenetre");

    while(menufenetre.isOpen())
    {
        Event event;

        while (menufenetre.pollEvent(event))
        {

            switch (event.type)
            {

            case Event::Closed:
                menufenetre.close();
                break;

            // gere le clique gauche de la souris dans le menu
            case Event::MouseButtonPressed:
                if (event.mouseButton.button == Mouse::Left && fenetreaffiche==1) //correspond aux levels
                {
                    if ( (280  <=  event.mouseButton.x) && ( event.mouseButton.x   <= 480) )
                    {
                        if     ((330  <= event.mouseButton.y) && (event.mouseButton.y <= 360 ) )
                        {
                            level=1;
                            printf("%i", level);
                            menufenetre.close();

                            One.healthPoint=4;
                            One.speedShipEnnemy=0.2;
                            One.step=0.45;
                            One.numberEnnemy=10;

                        }
                        else if     ((612  <= event.mouseButton.y) && (event.mouseButton.y <= 642 ) )
                        {
                            level=2;
                            printf("%i", level);
                            menufenetre.close();

                            One.healthPoint=3;
                            One.speedShipEnnemy=0.35;
                            One.step=0.5;
                            One.numberEnnemy=20;

                        }
                    }
                    else if ( (216  <=  event.mouseButton.x) && ( event.mouseButton.x   <= 546) )
                    {
                        if ((470  <= event.mouseButton.y) && (event.mouseButton.y <= 500 ) )
                        {
                            level=3;
                            printf("%i", level);

                        }
                    }
                }
                if (event.mouseButton.button == Mouse::Left && fenetreaffiche==0) //correspond au menu jouer
                {
                    if ( (280  <=  event.mouseButton.x) && ( event.mouseButton.x   <= 530) )
                    {
                        if     ((350  <= event.mouseButton.y) && (event.mouseButton.y <= 450 ) )
                        {
                            printf("pressed toto"); //affiche le choix des difficult�s
                            affichagelevel(menufenetre);
                            fenetreaffiche=1;
                        }
                        else if ((600  <= event.mouseButton.y) && (event.mouseButton.y <= 700 ))
                        {
                            menufenetre.close();

                        }
                    }
                    if (fenetreaffiche == 1)
                    {
                        menufenetre.draw(diff);
                    }
                }
                break;
            }
        }

        if (fenetreaffiche == 0)
        {
            affichagemenu(menufenetre);
        }

    }




    //cr�ation de la fen�tre
    RenderWindow fenetre(VideoMode(SIZE_WINDOW, SIZE_WINDOW), "SPACE SHOOTER");
    //deuxieme fenetre de jeu
    while (fenetre.isOpen())
    {

        fenetre.clear(Color::Black);


        for (i=0; i<One.numberEnnemy; i++)
        {

            ennemyShip.posY = 0;
            ennemyShip.posX = rand() %(SIZE_WINDOW-WIDTH_ENNEMY);
            choix = rand()%2+1;

            if (choix==1)
            {
                ennemyOne.setTexture(picture);
            }

            else
                ennemyOne.setTexture(pictureTwo);

            while (ennemyShip.posY <= SIZE_WINDOW)
            {

                ennemyShip.posY = ennemyShip.posY+One.speedShipEnnemy;
                ennemyOne.setPosition(ennemyShip.posX,ennemyShip.posY);


                //les events
                while (fenetre.pollEvent(evenement))
                {

                    switch (evenement.type)
                    {

                    case Event::Closed:
                        fenetre.close();
                        break;


                    case Event::MouseButtonPressed:
                        if (evenement.mouseButton.button == Mouse::Left)
                        {

                            test=1;
                            infoBullet.posX = theShip.posX+30;
                            infoBullet.posY = theShip.posY;
                            printf("Tir \n");
                            // A DEFINIR DANS UNE FONCTION
                            //modifier mettre 15 cliques max pour les autres niveaux
                            bulletShip.setTexture(imageBullet);
                            bulletShip.setPosition(infoBullet.posX,infoBullet.posY);

                        }
                    }
                }


                if (Keyboard::isKeyPressed(Keyboard::Up))
                {
                    if (theShip.posY > 100)
                    {
                        theShip.posY = theShip.posY-One.step;
                    }

                }
                if (Keyboard::isKeyPressed(Keyboard::Down))
                {
                    if (theShip.posY <= SIZE_WINDOW-SIZE_SHIP)
                    {
                        theShip.posY = theShip.posY+One.step;
                    }

                }
                if (Keyboard::isKeyPressed(Keyboard::Left))
                {
                    if (theShip.posX >= 0)
                    {
                        theShip.posX = theShip.posX-One.step;
                    }

                }
                if (Keyboard::isKeyPressed(Keyboard::Right))
                {
                    if (theShip.posX <= SIZE_WINDOW-SIZE_SHIP)
                    {
                        theShip.posX = theShip.posX+One.step;
                    }

                }


                //test des tirs avec incr�mentation des d�placement des bullets

                if (test==1)
                {
                    infoBullet.posY-=5;
                    if (infoBullet.posY<=-60)
                        test=0;
                }

                //test des colisions haut bas droite gauche

                if (((infoBullet.posY<ennemyShip.posY+SIZE_SHIP) && (infoBullet.posY>ennemyShip.posY)) && ((infoBullet.posX<ennemyShip.posX+SIZE_SHIP) && (infoBullet.posX>ennemyShip.posX)))
                {
                    printf("bingo");
                    ennemyShip.posY = SIZE_WINDOW;
                }




                if (((theShip.posY<ennemyShip.posY+SIZE_SHIP) && (theShip.posY>ennemyShip.posY)) && ((theShip.posX+(SIZE_SHIP/2)<ennemyShip.posX+SIZE_SHIP) && (theShip.posX+(SIZE_SHIP/2)>ennemyShip.posX)))
                {
                    printf("GAME OVER 1\n");
                    //AJOUTER LIEN Fontion CALCULE HP
                    theShip.posX=centre;
                    theShip.posY=centre;
                    //colision haut vaiseau bas ennemy
                }



                if (((theShip.posY+SIZE_SHIP>ennemyShip.posY) && (theShip.posY<ennemyShip.posY)) && ((theShip.posX+(SIZE_SHIP/2)<ennemyShip.posX+SIZE_SHIP) && (theShip.posX+(SIZE_SHIP/2)>ennemyShip.posX)))
                {
                    printf("GAME OVER 2\n");
                    //AJOUTER LIEN Fontion CALCULE HP
                    theShip.posX=centre;
                    theShip.posY=centre;
                    //colision haut vaiseau bas ennemy
                }



                //setposition des images et draw des images.

                bulletShip.setPosition(infoBullet.posX,infoBullet.posY);
                shipInGame.setTexture(image);
                shipInGame.setPosition(theShip.posX,theShip.posY);
                fenetre.clear(Color::Black);
                backgrd.setTexture(background);
                fenetre.draw(backgrd);
                fenetre.draw(bulletShip);
                fenetre.draw(ennemyOne);
                fenetre.draw(shipInGame);
                fenetre.display();

            }

        }
        break;
    }
    return 0;
}


//d�claration des fonctions

void affichagelevel(RenderWindow& menufenetre)
{

    Texture Image2;
    Image2.loadFromFile("img/menu2.png");
    Sprite Spriteimagefond2(Image2);
    Spriteimagefond2.scale(0.7f, 0.6f);
    Spriteimagefond2.setOrigin(60,50);
    menufenetre.clear();
    menufenetre.draw(Spriteimagefond2);
    menufenetre.display();

}


void affichagemenu(RenderWindow& menufenetre)
{
    Texture Image;
    Image.loadFromFile("img/menu1.png");
    Sprite Spriteimagefond(Image);
    Spriteimagefond.scale(0.7f, 0.6f);
    Spriteimagefond.setOrigin(60,50);

    RectangleShape difficulte(Vector2f (250, 100));
    difficulte.setFillColor(Color(255, 255, 255));
    difficulte.setPosition(280, 350);

    menufenetre.clear();
    menufenetre.draw(Spriteimagefond);
    menufenetre.display();
}


