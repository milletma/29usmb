#include <SFML/Graphics.hpp>
#include <time.h>
#include <SFML/Audio.hpp>
#include "fonctions.hpp"

using namespace sf;

int main()
{
    while(1)
    {

        int ennemyK, shotFired, healthPoint, x, y, level,choix, i, test;
        int damage=0, point=0,breaktotal=0, victoire=0, fenetreaffiche=0;
        int centre = (SIZE_WINDOW - SIZE_SHIP)/2;

        //d�claration des structures
        Bullet infoBullet;
        Ship theShip;
        Ship ennemyShip;
        Level One;

        // Liens des musiques
        Music mmenu,ingame,explosion,shoot;
        if(!mmenu.openFromFile("sons/mainmenu.wav"))
            printf("Error opening mmenu file");

        if(!ingame.openFromFile("sons/songingame.wav"))
            printf("Error opening ingame file");

        if(!explosion.openFromFile("sons/explosion.ogg"))
            printf("Error opening explosion file");

        if(!shoot.openFromFile("sons/pew.ogg"))
            printf("Error opening shoot file");


        //liens et d�finition des textures;
        Texture image;
        if (!image.loadFromFile("img/ship.png"))
            printf("Error loading ship pitcure !\n");
        Sprite shipInGame;
        shipInGame.setTexture(image);
        shipInGame.setPosition(theShip.posX,theShip.posY);

        Texture imageBullet;
        if (!imageBullet.loadFromFile("img/bullet.png"))
            printf("Error loading ship bullet picture !");
        Sprite bulletShip;
        bulletShip.setTexture(imageBullet);
        bulletShip.setPosition(infoBullet.posX,infoBullet.posY);

        Texture picture;
        if (!picture.loadFromFile("img/ennemyship.png"))
            printf("Error loading ennemyship picture !\n");
        Sprite ennemyOne;

        Texture pictureTwo;
        if (!pictureTwo.loadFromFile("img/ennemyshiptwo.png"))
            printf("Error loading ennemyshiptwo picture !\n");
        Sprite ennemyTwo;

        Texture background;
        if(!background.loadFromFile("img/bckgrd.jpg"));
        printf("Error loading background picture !");
        Sprite backgrd;


        //initialisation des positions de d�parts
        theShip.posX = centre;
        theShip.posY = centre+(centre/2);
        ennemyShip.posX = 0;
        ennemyShip.posY = 0;
        infoBullet.posX = theShip.posX;
        infoBullet.posY = -60;

        //d�claration des autres trucs
        Event evenement;
        srand(time(NULL));

        //d�claration des shapes pour la 1er fenetre
        RectangleShape diff(Vector2f (250, 100));
        diff.setFillColor(Color(255, 255, 255));
        diff.setPosition(280, 350);

        //lancement 1er fen�tre
        RenderWindow menufenetre(VideoMode(800, 800), "MENU");

        mmenu.play(); //lancement musique menu
        mmenu.setLoop(true); //lancement musique menu en boucle

        while(menufenetre.isOpen())
        {
            Event event;
            while (menufenetre.pollEvent(event))
            {

                switch (event.type)
                {

                case Event::Closed:
                    exit(0);

                // gere le clique gauche de la souris dans le menu
                case Event::MouseButtonPressed:
                    if (event.mouseButton.button == Mouse::Left && fenetreaffiche==1) //correspond aux levels
                    {
                        if ( (280  <=  event.mouseButton.x) && ( event.mouseButton.x   <= 480) )
                        {
                            if     ((330  <= event.mouseButton.y) && (event.mouseButton.y <= 360 ) )
                            {
                                //initialisation du level 1
                                level=1;
                                menufenetre.close();
                                healthPoint=4;
                                One.speedShipEnnemy=1;
                                One.step=1.2;
                                One.numberEnnemy=15;
                            }
                            else if     ((612  <= event.mouseButton.y) && (event.mouseButton.y <= 642 ) )
                            {
                                //initialisation du level 3
                                level=3;
                                menufenetre.close();
                                healthPoint=1;
                                One.speedShipEnnemy=2;
                                One.step=2.1;
                                One.numberEnnemy=60;

                            }
                        }
                        if ( (216  <=  event.mouseButton.x) && ( event.mouseButton.x   <= 546) )
                        {
                            if ((470  <= event.mouseButton.y) && (event.mouseButton.y <= 500 ) )
                            {
                                //initialisation du level 2
                                level=2;
                                menufenetre.close();
                                healthPoint=3;
                                One.speedShipEnnemy=1.5;
                                One.step=1.6;
                                One.numberEnnemy=30;

                            }
                        }
                    }
                    if (event.mouseButton.button == Mouse::Left && fenetreaffiche==0) //correspond au menu jouer
                    {
                        if ( (280  <=  event.mouseButton.x) && ( event.mouseButton.x   <= 530) )
                        {
                            if     ((350  <= event.mouseButton.y) && (event.mouseButton.y <= 450 ) )
                            {
                                //affiche le choix des difficult�s
                                affichagelevel(menufenetre);
                                fenetreaffiche=1;
                            }
                            else if ((600  <= event.mouseButton.y) && (event.mouseButton.y <= 700 ))
                            {
                                menufenetre.close();
                                breaktotal=1;
                                return EXIT_SUCCESS;
                            }
                        }
                        if (fenetreaffiche == 1)
                        {
                            menufenetre.draw(diff);
                        }
                    }
                    break;
                }
            }

            if (fenetreaffiche == 0)
            {
                affichagemenu(menufenetre);
            }

        }
        mmenu.stop(); //STOP Musique du menu

        Texture four,three,one;
        if (level==1)
        {
            if (!four.loadFromFile("img/HP4.png"))
                printf("WARNING , can t load !\n");
        }
        if (level==2)
        {
            if (!three.loadFromFile("img/HP3.png"))
                printf("WARNING , can t load !\n");
        }
        if (level==3)
        {
            if (!one.loadFromFile("img/HP1.png"))
                printf("WARNING , can t load !\n");
        }
        Sprite hpFour,hpThree,hpOne;
        hpFour.setPosition(20,740);
        hpFour.setTexture(four);
        hpThree.setPosition(20,740);
        hpThree.setTexture(three);
        hpOne.setPosition(20,740);
        hpOne.setTexture(one);



        //cr�ation de la fen�tre de jeu
        RenderWindow fenetre(VideoMode(SIZE_WINDOW, SIZE_WINDOW), "SPACE SHOOTER");

        while (fenetre.isOpen())
        {

            fenetre.clear(Color::Black);
            ingame.play(); //Play musique jeu
            ingame.setLoop(true);


            for (i=0; i<One.numberEnnemy; i++)
            {
                if(breaktotal==1)
                    break;

                ennemyShip.posY = 0;
                ennemyShip.posX = rand() %(SIZE_WINDOW-WIDTH_ENNEMY);
                choix = rand()%2+1;

                if (choix==1)
                {
                    ennemyOne.setTexture(picture);
                }

                else
                    ennemyOne.setTexture(pictureTwo);

                while (ennemyShip.posY <= SIZE_WINDOW)
                {

                    ennemyShip.posY = ennemyShip.posY+One.speedShipEnnemy;
                    ennemyOne.setPosition(ennemyShip.posX,ennemyShip.posY);


                    //les events
                    while (fenetre.pollEvent(evenement))
                    {

                        switch (evenement.type)
                        {

                        case Event::Closed:
                            fenetre.close();
                            break;
                            return EXIT_SUCCESS;


                        case Event::MouseButtonPressed:
                            if (evenement.mouseButton.button == Mouse::Left)
                            {
                                shoot.play(); //PLAY BRUIT DE SHOOT
                                test=1;
                                infoBullet.posX = theShip.posX+30;
                                infoBullet.posY = theShip.posY;
                                shotFired=1;
                                bulletShip.setTexture(imageBullet);
                                bulletShip.setPosition(infoBullet.posX,infoBullet.posY);

                            }
                        }
                    }

                    processMouvement(theShip, One);//d�placement vaisseau ennemie

                    //test des tirs avec incr�mentation des d�placement des bullets

                    if (test==1)
                    {
                        infoBullet.posY-=20;
                        if (infoBullet.posY<=-60)
                            test=0;
                    }

                    if(ennemyShip.posY >= SIZE_WINDOW)
                        point=point-100;

                    //test des colisions haut bas droite gauche
                    if (((infoBullet.posY<ennemyShip.posY+(SIZE_SHIP/2)) && (infoBullet.posY>ennemyShip.posY)) && ((infoBullet.posX<ennemyShip.posX+SIZE_SHIP) && (infoBullet.posX>ennemyShip.posX)))
                    {
                        explosion.play(); //BRUIT D'EXPLOSION
                        ennemyShip.posY = SIZE_WINDOW+1;
                        infoBullet.posY = -60;
                        ennemyK=1;
                    }


                    if (((theShip.posY+SIZE_SHIP-1>ennemyShip.posY) && (theShip.posY<ennemyShip.posY)) && ((theShip.posX+(SIZE_SHIP/2)<ennemyShip.posX+SIZE_SHIP) && (theShip.posX+(SIZE_SHIP/2)>ennemyShip.posX)))
                    {
                        damage=1;
                        theShip.posX=centre;
                        theShip.posY=centre;
                    }

                    //setposition des images et draw des images.
                    bulletShip.setPosition(infoBullet.posX,infoBullet.posY);
                    shipInGame.setTexture(image);
                    shipInGame.setPosition(theShip.posX,theShip.posY);
                    fenetre.clear(Color::Black);
                    backgrd.setTexture(background);
                    fenetre.draw(backgrd);
                    score(fenetre,ennemyK,shotFired,point);
                    ennemyK=0;
                    shotFired=0;
                    fenetre.draw(bulletShip);
                    fenetre.draw(ennemyOne);
                    fenetre.draw(shipInGame);

                    if (healthPoint == 4)
                        fenetre.draw(hpFour);
                    if (healthPoint == 3)
                        fenetre.draw(hpThree);
                    if (healthPoint == 1)
                        fenetre.draw(hpOne);

                    if (healthPoint ==0)
                    {
                        victoire=1;
                        affichagefin2(fenetre);
                        breaktotal=1;
                    }

                    if (breaktotal == 1)
                    {
                        fenetre.close();
                        break;
                    }

                    hpLeft(healthPoint,damage,fenetre);//v�rification des points de vie et des dommage re�u
                    damage=0;
                    fenetre.display();
                }
            }
            affichagefin(fenetre);
            fenetre.close();
        }
        ingame.stop();
    }

    return 0;
}

