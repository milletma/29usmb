#define WIDTH_ENNEMY 60
#define HEIGHT_ENNEMY 60
#define SIZE_WINDOW 800
#define SIZE_SHIP 80
#define SIZE_BULLET_height 60
#define SIZE_BULLET_width 20


typedef struct
{
    float posX;
    float posY;

} Bullet;

typedef struct
{
    float posX;
    float posY;

} Ship;

typedef struct
{
    float step;
    float speedShipEnnemy;
    int healthPoint;
    int numberEnnemy;

} Level;
