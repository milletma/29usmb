#include <SFML/Graphics.hpp>
#include <time.h>
#include <SFML/Audio.hpp>
#include "structures.hpp"

using namespace sf;

//d�claration des fonctions

void affichagelevel(RenderWindow& menufenetre) //affiche les diff�rents level disponible
{

    Texture Image2;
    Image2.loadFromFile("img/menu2.png");
    Sprite Spriteimagefond2(Image2);
    Spriteimagefond2.scale(0.7f, 0.6f);
    Spriteimagefond2.setOrigin(60,50);
    menufenetre.clear();
    menufenetre.draw(Spriteimagefond2);
    menufenetre.display();

}

void affichagemenu(RenderWindow& menufenetre)  //affiche le menu du jeu
{
    Texture Image;
    Image.loadFromFile("img/menu1.png");
    Sprite Spriteimagefond(Image);
    Spriteimagefond.scale(0.7f, 0.6f);
    Spriteimagefond.setOrigin(60,50);

    RectangleShape difficulte(Vector2f (250, 100));
    difficulte.setFillColor(Color(255, 255, 255));
    difficulte.setPosition(280, 350);

    menufenetre.clear();
    menufenetre.draw(Spriteimagefond);
    menufenetre.display();
}


void score(RenderWindow & fenetre,int ennemyK,int shotFired,int &point) //affiche le score
{
    Font font;
    if (!font.loadFromFile("techno_hideo.ttf"))
        printf("Pb load");

    Text text;
    text.setFont(font);

    if (shotFired==1)
        point=point-20;
    if(point <=0)
        point=0;
    if (ennemyK==1)
        point=point+100;

    Text textScore, textDisplay;
    textDisplay.setFont(font);
    textDisplay.setPosition(0,30);
    char affScore [50];
    sprintf(affScore,"Score : %i",point);
    textScore.setString(affScore);
    textScore.setFont(font);
    textScore.setStyle(Text::Bold);
    textScore.setPosition(520,700);
    fenetre.draw(textScore);
}


int hpLeft (int &healthPoint,int damage,RenderWindow &fenetre) //r�cup�re les points de vie et les dommges subit et les recalcule en focntion des �venements
{
    Font font;
    if (!font.loadFromFile("techno_hideo.ttf"))
        printf("Fatal error: techno_hideo.ttf missing");
    Text text;
    text.setFont(font);

    Text HPdis, textDisplay;
    textDisplay.setFont(font);
    textDisplay.setPosition(50,700);
    char HPwrite [50];
    sprintf(HPwrite,"HP left :");
    HPdis.setString(HPwrite);
    HPdis.setFont(font);
    HPdis.setStyle(Text::Bold);
    HPdis.setPosition(20,700);
    fenetre.draw(HPdis);

    Texture three,two,one,go;

    if (damage==1)
        healthPoint=healthPoint-1;

    if (healthPoint==3)
    {
        if (!three.loadFromFile("img/HP3.png"))
            printf("WARNING , can t load !\n");
        Sprite hpThree;
        hpThree.setPosition(20,740);
        hpThree.setTexture(three);
        fenetre.draw(hpThree);
    }

    if  (healthPoint==2)
    {
        if (!two.loadFromFile("img/HP2.png"))
            printf("WARNING , can t load !\n");
        Sprite hpTwo;
        hpTwo.setPosition(20,740);
        hpTwo.setTexture(two);
        fenetre.draw(hpTwo);
    }

    if (healthPoint==1)
    {
        if (!one.loadFromFile("img/HP1.png"))
            printf("WARNING , can t load !\n");
        Sprite hpOne;
        hpOne.setPosition(20,740);
        hpOne.setTexture(one);
        fenetre.draw(hpOne);
    }

    if (healthPoint==0)
    {
        if (!one.loadFromFile("img/gameover.png"))
            printf("WARNING , can t load !\n");
        Sprite gameOver;
        gameOver.setTexture(go);
        fenetre.draw(gameOver);
    }

    return healthPoint;
}

void affichagefin(RenderWindow& fenetre) //affiche la fen�tre de victoire
{
    Texture Image3;
    Image3.loadFromFile("img/victory.png");
    Sprite Spriteimagefond(Image3);
    fenetre.clear();
    fenetre.draw(Spriteimagefond);
    fenetre.display();
    sleep(seconds(2));
}

void affichagefin2(RenderWindow& fenetre) //affiche la fen�tre game over
{
    fenetre.clear();
    Texture Image3;
    Image3.loadFromFile("img/gameover.png");
    Sprite Spriteimagefond(Image3);
    fenetre.draw(Spriteimagefond);
    fenetre.display();
    sleep(seconds(2));
}


void processMouvement(Ship &theShip, Level &One) //analyse les diff�rents touche des fl�ches directionel en jeu
{

    if (Keyboard::isKeyPressed(Keyboard::Up))
    {
        if (theShip.posY > 100)
        {
            theShip.posY = theShip.posY-One.step;
        }

    }
    if (Keyboard::isKeyPressed(Keyboard::Down))
    {
        if (theShip.posY <= SIZE_WINDOW-SIZE_SHIP)
        {
            theShip.posY = theShip.posY+One.step;
        }

    }
    if (Keyboard::isKeyPressed(Keyboard::Left))
    {
        if (theShip.posX >= 0)
        {
            theShip.posX = theShip.posX-One.step;
        }

    }
    if (Keyboard::isKeyPressed(Keyboard::Right))
    {
        if (theShip.posX <= SIZE_WINDOW-SIZE_SHIP)
        {
            theShip.posX = theShip.posX+One.step;
        }

    }
}

