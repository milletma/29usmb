#include <SFML/Graphics.hpp>
using namespace sf;

void affichagelevel(RenderWindow& fenetre);
void affichagemenu (RenderWindow& fenetre);

void affichagelevel(RenderWindow& fenetre)
{
    Texture Image2;
    Image2.loadFromFile("menu2.png");
    Sprite Spriteimagefond2(Image2);
    Spriteimagefond2.scale(0.7f, 0.6f);
    Spriteimagefond2.setOrigin(60,50);
    fenetre.clear();
    fenetre.draw(Spriteimagefond2);
    fenetre.display();
}
void affichagemenu(RenderWindow& fenetre)
{
    Texture Image;
    Image.loadFromFile("menu1.png");
    Sprite Spriteimagefond(Image);
    Spriteimagefond.scale(0.7f, 0.6f);
    Spriteimagefond.setOrigin(60,50);


    RectangleShape difficulte(Vector2f (250, 100));
    difficulte.setFillColor(Color(255, 255, 255));
    difficulte.setPosition(280, 350);

    fenetre.clear();
    fenetre.draw(Spriteimagefond);
    fenetre.display();
}
int main(int argc, char *argv[])
{
    RectangleShape diff(Vector2f (250, 100));
    diff.setFillColor(Color(255, 255, 255));
    diff.setPosition(280, 350);

    int x;
    int y;
    int level;
    int fenetreaffiche=0;
    RenderWindow fenetre(sf::VideoMode(800, 800), "fenetre");
    while(fenetre.isOpen())
    {
        Event event;

        while (fenetre.pollEvent(event))
        {

            switch (event.type)
            {

            case ::Event::Closed:
                fenetre.close();
                break;

            // gere le clique gauche de la souris dans le menu
            case Event::MouseButtonPressed:
                if (event.mouseButton.button == Mouse::Left && fenetreaffiche==1) //correspond aux levels
                {
                    if ( (280  <=  event.mouseButton.x) && ( event.mouseButton.x   <= 480) )
                    {
                        if     ((330  <= event.mouseButton.y) && (event.mouseButton.y <= 360 ) )
                        {

                            level=1;
                            // printf("%i", level);
                        }

                        else if     ((612  <= event.mouseButton.y) && (event.mouseButton.y <= 642 ) )
                        {
                            level=2;
                            //printf("%i", level);
                        }


                    }
                    else if ( (216  <=  event.mouseButton.x) && ( event.mouseButton.x   <= 546) )
                    {

                        if ((470  <= event.mouseButton.y) && (event.mouseButton.y <= 500 ) )
                        {
                            level=3;
                            //  printf("%i", level);
                        }
                    }
                }
                if (event.mouseButton.button == Mouse::Left && fenetreaffiche==0) //correspond au menu jouer
                {
                    if ( (280  <=  event.mouseButton.x) && ( event.mouseButton.x   <= 530) )
                    {
                        if     ((350  <= event.mouseButton.y) && (event.mouseButton.y <= 450 ) )
                        {
                            printf("pressed toto"); //affiche le choix des difficultés

                            affichagelevel(fenetre);
                            fenetreaffiche=1;
                        }

                        else if ((600  <= event.mouseButton.y) && (event.mouseButton.y <= 700 ))
                        {
                            fenetre.close();
                        }
                    }
                    if (fenetreaffiche == 1)
                    {
                        fenetre.draw(diff);
                    }
                }


                break;
            }
        }

        if (fenetreaffiche == 0)
        {
            affichagemenu(fenetre);
        }

    }

    return 0;

}
